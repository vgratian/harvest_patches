

## Harvest Hotfix 1410

Fixes:
* BUG missing normalization of wafl_block in gb_per_sec (kuddos to @yannb for finding and solving this)
* BUG miscalculating link_speed which breaks ethernet port utilization statistics.

Requires:
* NetApp Harvest 1.4.1
* Python2 or higher

Usage:
1. Copy the file to `/tmp/` (or any other folder) on your Harvest server.
2. Stop Harvest (`./netapp-manager -stop`)
3. Run the file:
 * If Harvest is installed in default location (`/opt/netapp-harvest/`), simply run in terminal:
 ```sh
 $ ./harvest_hotfix_1410
 ```
 * If otherwise Harvest is in another location, provide Harvest path as argument, e.g.:
 terminal:
 ```sh
 $ ./harvest_hotfix_1410 /home/admin/netapp-harvest/
 ```     
4. Read the output and see if the update was successful
5. Restart Harvest (`./netapp-manager -start`)
